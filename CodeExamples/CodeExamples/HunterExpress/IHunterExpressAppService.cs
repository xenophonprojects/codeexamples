﻿using Abp.Application.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Carrier.Dto;
using HunterExpress.Dto;
using Abp.Domain.Services;
using CustomerOrder;

namespace ..HunterExpress
{
    public interface IHunterExpressDomainService : IDomainService
    {
        Task<List<HunterExpressServicePriceDto>> GetProductRates(List<HunterExpressServicePriceDto> hunterExpressServicePrice);
        Task<CarrierBookingDto> ValidateConsignment(CarrierBookingDto carrierConsignmentDto);
        Task<CarrierBookingDto> CreateBooking(CarrierBookingDto bookingInput);
        Task<HunterExpressStatusResultDto> PollForTrackingUpdates(string trackingNumber, int apiId);
        Task<string> GetHunterExpressPackageType(string PackageType);
        Task<bool> CancelConsignments(List<HunterExpressRefundDto> orders);
    }
}