using HunterExpress.Dto;
using Newtonsoft.Json;
using RestSharp;
using RestSharp.Authenticators;
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;

namespace HunterExpress.Api
{
    class HunterExpressAPI
    {
        public string _hunterExpressAccountNumber;
        string _hunterExpressAPIKey;
        public string _hunterExpressAPIPassword { private get; set; }
        string _hunterExpressBaseUrl;
        public int _hunterExpressAPIId { get; private set; }
        public decimal _hunterExpressMaxArticleWeight { get; set; }

        public HunterExpressAPI()
        {

        }

        public HunterExpressAPI(string hunterExpressAPIKey, string hunterExpressAPIPassword, string hunterExpressBaseUrl, int hunterExpressApiId, decimal hunterExpressMaxArticleWeight = 0m)
        {
            _hunterExpressAccountNumber = hunterExpressAPIKey;
            _hunterExpressAPIKey = hunterExpressAPIKey;
            _hunterExpressAPIPassword = hunterExpressAPIPassword;
            _hunterExpressBaseUrl = hunterExpressBaseUrl;
            _hunterExpressAPIId = hunterExpressApiId;
            _hunterExpressMaxArticleWeight = hunterExpressMaxArticleWeight;
        }

        private RestClient CreateHunterExpressRestClient()
        {
            var client = new RestClient(_hunterExpressBaseUrl);
            client.Authenticator = new HttpBasicAuthenticator(_hunterExpressAPIKey, _hunterExpressAPIPassword);

            return client;
        }

        public async Task<List<HunterExpressServicePriceResultDto>> GetDomesticQuote(HunterExpressServicePriceRequestDto hunterExpressServicePriceRequest)
        {
            var client = CreateHunterExpressRestClient();
            var request = new RestRequest("quote/get-quote", Method.POST);
            request.AddHeader("Accept", "application/json");
            request.AddHeader("Content-Type", "application/json");
            request.AddJsonBody(hunterExpressServicePriceRequest);

            IRestResponse response = client.Execute(request);
            if (response.StatusCode != HttpStatusCode.OK)
            {
                string prettyErrorMessage = await GetPrettyErrorMessage(response);
                throw new Exception(prettyErrorMessage);
            }

            List<HunterExpressServicePriceResultDto> data = new List<HunterExpressServicePriceResultDto>();
            data = JsonConvert.DeserializeObject<List<HunterExpressServicePriceResultDto>>(response.Content);

            return data;
        }

        public async Task<HunterExpressBookingReturnDto> BookDomesticPickup(HunterExpressBookingRequestDto hunterExpressBookingRequest)
        {
            var client = CreateHunterExpressRestClient();
            HunterExpressBookingReturnDto data = new HunterExpressBookingReturnDto();

            var request = new RestRequest("booking/book-job", Method.POST);
            request.AddHeader("Accept", "application/json");
            request.AddHeader("Content-Type", "application/json");
            request.AddJsonBody(hunterExpressBookingRequest);

            IRestResponse response = client.Execute(request);
            if (response.StatusCode != HttpStatusCode.OK)
            {
                string prettyErrorMessage = await GetPrettyErrorMessage(response);
                throw new Exception(prettyErrorMessage);
            }

            return JsonConvert.DeserializeObject<HunterExpressBookingReturnDto>(response.Content);
        }

        public async Task<HunterExpressStatusResultDto> GetConsignmentTracking(string trackingNumber)
        {
            var client = CreateHunterExpressRestClient();

            var request = new RestRequest("booking/get-job-statuses", Method.GET);
            request.AddHeader("Accept", "application/json");
            request.AddHeader("Content-Type", "application/json");
            request.AddParameter("customerCode", _hunterExpressAccountNumber);
            request.AddParameter("trackingNumber", trackingNumber);

            IRestResponse response = client.Execute(request);
            if (response.StatusCode != HttpStatusCode.OK)
            {
                string prettyErrorMessage = await GetPrettyErrorMessage(response);
                throw new Exception(prettyErrorMessage);
            }

            return JsonConvert.DeserializeObject<HunterExpressStatusResultDto>(response.Content);
        }

        private async Task<string> GetPrettyErrorMessage(IRestResponse response)
        {
            HunterExpressAPIErrorDto error = JsonConvert.DeserializeObject<HunterExpressAPIErrorDto>(response.Content);
            return string.Format("{0} - {1}", error.errorCode, error.errorMessage);
        }
    }
}
