﻿using Abp.Domain.Repositories;
using Abp.Extensions;
using Carrier.Dto;
using Configuration;
using Configuration.Host.Dto;
using Email;
using HunterExpress.Api;
using HunterExpress.Dto;
using HunterExpressAccounts;
using HunterExpressLocationMaster;
using PackageType;
using Surcharge;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HunterExpress
{
    public class HunterExpressDomainService : DomainServiceBase, IHunterExpressDomainService
    {
        public IRepository<HunterExpressLocationMaster> _hunterExpressLocationMasterRepository;
        public IRepository<HunterExpressAccounts> _hunterExpressAccountsRepository;
        private readonly IRepository<PackageType> _packageTypeRepository;
        public IEmailAppService _emailAppService;

        public HunterExpressDomainService(IRepository<HunterExpressLocationMaster> hunterExpressLocationMasterRepository,
            IRepository<HunterExpressAccounts> hunterExpressAccountsRepository,
            IRepository<PackageType> packageTypeRepository,
            IEmailAppService emailAppService)
        {
            _hunterExpressLocationMasterRepository = hunterExpressLocationMasterRepository;
            _hunterExpressAccountsRepository = hunterExpressAccountsRepository;
            _packageTypeRepository = packageTypeRepository;
            _emailAppService = emailAppService;
        }

        public static class HunterExpressAPIZoneConstants
        {
            public const int Sydney = 62;
            public const int Melbourne = 49;
            public const int Brisbane = 1;
        }

        public static class HunterExpressPackageTypeConstants
        {
            public const string Pallet = "PLT";
            public const string Carton = "CTN";
            public const string Satchel = "PPS";
        }

        public static class HunterExpressServiceTypeConstants
        {
            public const string RoadFreight = "RF";
            public const string AirFreight = "AF";
            public const string SameDayAirFreight = "SDX";
            public const string HomeDeliveryPlus = "HDP";
        }

        /* 
         * API Selection Methods
         */

        private async Task<HunterExpressAPI> GetHunterExpressAPIInstance(int metroZone, string packageType)
        {
            try
            {
                HunterExpressAPI apiInstance = new HunterExpressAPI();

                var apiAccount = await _hunterExpressAccountsRepository.FirstOrDefaultAsync(a => a.HunterExpressZoneId == metroZone && a.PackageType == packageType.ToUpper());

                if (apiAccount == null)
                {
                    return null;
                }

                if (apiAccount.HunterExpressZoneId == HunterExpressAPIZoneConstants.Sydney)
                {
                    if (apiAccount.PackageType == HunterExpressPackageTypeConstants.Pallet)
                    {
                        apiInstance = await GetHunterExpressExSydneyPalletAPICredentials(apiAccount.Id);
                    }
                    else if (apiAccount.PackageType == HunterExpressPackageTypeConstants.Carton)
                    {
                        apiInstance = await GetHunterExpressExSydneyCartonAPICredentials(apiAccount.Id);
                    }
                }
                else if (apiAccount.HunterExpressZoneId == HunterExpressAPIZoneConstants.Melbourne)
                {
                    if (apiAccount.PackageType == HunterExpressPackageTypeConstants.Pallet)
                    {
                        apiInstance = await GetHunterExpressExMelbournePalletAPICredentials(apiAccount.Id);
                    }
                    else if (apiAccount.PackageType == HunterExpressPackageTypeConstants.Carton)
                    {
                        apiInstance = await GetHunterExpressExMelbourneCartonAPICredentials(apiAccount.Id);
                    }
                }
                else if (apiAccount.HunterExpressZoneId == HunterExpressAPIZoneConstants.Brisbane)
                {
                    apiInstance = await GetHunterExpressExBrisbaneAPICredentials(apiAccount.Id);
                }
                else
                {
                    return null;
                }

                return apiInstance;
            }
            catch
            {
                return null;
            }
        }

        //Ex Sydney Pallet Rates
        private async Task<HunterExpressAPI> GetHunterExpressExSydneyPalletAPICredentials(int apiId)
        {
            var key = await SettingManager.GetSettingValueAsync(HunterExpressSettingsNames.HunterExpressExSydneyPalletAPIKey);
            var password = await SettingManager.GetSettingValueAsync(HunterExpressSettingsNames.HunterExpressExSydneyPalletAPIPassword);
            var baseUrl = await SettingManager.GetSettingValueAsync(HunterExpressSettingsNames.HunterExpressBaseUrl);
            return new HunterExpressAPI(key, password, baseUrl, apiId);
        }

        //Ex Sydney Carton Rates
        private async Task<HunterExpressAPI> GetHunterExpressExSydneyCartonAPICredentials(int apiId)
        {
            var key = await SettingManager.GetSettingValueAsync(HunterExpressSettingsNames.HunterExpressExSydneyCartonAPIKey);
            var password = await SettingManager.GetSettingValueAsync(HunterExpressSettingsNames.HunterExpressExSydneyCartonAPIPassword);
            var baseUrl = await SettingManager.GetSettingValueAsync(HunterExpressSettingsNames.HunterExpressBaseUrl);
            var maxCartonWeight = Convert.ToDecimal(await SettingManager.GetSettingValueAsync(HunterExpressSettingsNames.HunterExpressExSydneyCartonMaxKgCubic));
            return new HunterExpressAPI(key, password, baseUrl, apiId, maxCartonWeight);
        }

        //Ex Melbourne Pallet Rates
        private async Task<HunterExpressAPI> GetHunterExpressExMelbournePalletAPICredentials(int apiId)
        {
            var key = await SettingManager.GetSettingValueAsync(HunterExpressSettingsNames.HunterExpressExMelbournePalletAPIKey);
            var password = await SettingManager.GetSettingValueAsync(HunterExpressSettingsNames.HunterExpressExMelbournePalletAPIPassword);
            var baseUrl = await SettingManager.GetSettingValueAsync(HunterExpressSettingsNames.HunterExpressBaseUrl);
            return new HunterExpressAPI(key, password, baseUrl, apiId);
        }

        //Ex Melbourne Carton Rates
        private async Task<HunterExpressAPI> GetHunterExpressExMelbourneCartonAPICredentials(int apiId)
        {
            var key = await SettingManager.GetSettingValueAsync(HunterExpressSettingsNames.HunterExpressExMelbourneCartonAPIKey);
            var password = await SettingManager.GetSettingValueAsync(HunterExpressSettingsNames.HunterExpressExMelbourneCartonAPIPassword);
            var baseUrl = await SettingManager.GetSettingValueAsync(HunterExpressSettingsNames.HunterExpressBaseUrl);
            var maxCartonWeight = Convert.ToDecimal(await SettingManager.GetSettingValueAsync(HunterExpressSettingsNames.HunterExpressExMelbourneCartonMaxKgCubic));
            return new HunterExpressAPI(key, password, baseUrl, apiId, maxCartonWeight);
        }

        //Ex Brisbane "Best" Rates
        private async Task<HunterExpressAPI> GetHunterExpressExBrisbaneAPICredentials(int apiId)
        {
            var key = await SettingManager.GetSettingValueAsync(HunterExpressSettingsNames.HunterExpressExBrisbaneAPIKey);
            var password = await SettingManager.GetSettingValueAsync(HunterExpressSettingsNames.HunterExpressExBrisbaneAPIPassword);
            var baseUrl = await SettingManager.GetSettingValueAsync(HunterExpressSettingsNames.HunterExpressBaseUrl);
            var maxCartonWeight = Convert.ToDecimal(await SettingManager.GetSettingValueAsync(HunterExpressSettingsNames.HunterExpressExBrisbaneCartonMaxKgCubic));
            return new HunterExpressAPI(key, password, baseUrl, apiId, maxCartonWeight);
        }

        /*
         * Main Methods
         */

        public async Task<List<HunterExpressServicePriceDto>> GetProductRates(List<HunterExpressServicePriceDto> hunterExpressServicePrice)
        {
            foreach (var request in hunterExpressServicePrice)
            {
                try
                {
                    List<HunterExpressServicePriceResultDto> servicePriceResults = new List<HunterExpressServicePriceResultDto>();

                    //Hunter rates are reciprocal, so we do a price request with both sender and receiver suburb IF the suburbs are metro zones that we have rates for
                    int senderZone = await GetZoneFromSuburb(request.PriceRequest.fromLocation.suburbName, request.PriceRequest.fromLocation.postCode);
                    var senderResult = await GetQuote(request.PriceRequest, senderZone, request.OrderPackageType, request.ReceiverIsResidential);
                    if (senderResult != null)
                    {
                        servicePriceResults.AddRange(senderResult);
                    }

                    int receiverZone = await GetZoneFromSuburb(request.PriceRequest.toLocation.suburbName, request.PriceRequest.toLocation.postCode);
                    if (senderZone == receiverZone)
                    {
                        //End price lookup here as both sender and receiver are within the same zone
                        request.PriceResponse.AddRange(servicePriceResults);
                        return hunterExpressServicePrice;
                    }

                    //Swap the sender and receiver to do the reciprocal rate price request
                    var newSenderDetails = request.PriceRequest.fromLocation;
                    request.PriceRequest.fromLocation = request.PriceRequest.toLocation;
                    request.PriceRequest.toLocation = newSenderDetails;

                    var receiverResult = await GetQuote(request.PriceRequest, receiverZone, request.OrderPackageType, request.ReceiverIsResidential);

                    if (receiverResult != null)
                    {
                        servicePriceResults.AddRange(receiverResult);
                    }

                    //Create a distinct list of prices that contains the cheapest price for each service
                    if (senderResult != null && receiverResult != null)
                    {
                        var distinctServiceCodeList = servicePriceResults.Select(result => result.serviceCode).Distinct().ToList();

                        foreach (var service in distinctServiceCodeList)
                        {
                            var initialRate = senderResult.FirstOrDefault(i => i.serviceCode == service);
                            var reciprocalRate = receiverResult.FirstOrDefault(r => r.serviceCode == service);

                            if (initialRate == null)
                            {
                                request.PriceResponse.Add(reciprocalRate);
                            }
                            else if (reciprocalRate == null)
                            {
                                request.PriceResponse.Add(initialRate);
                            }
                            else
                            {
                                if (initialRate.fee <= reciprocalRate.fee)
                                {
                                    request.PriceResponse.Add(initialRate);
                                }
                                else
                                {
                                    request.PriceResponse.Add(reciprocalRate);
                                }
                            }
                        }
                    }
                    else
                    {
                        request.PriceResponse.AddRange(servicePriceResults);
                    }
                }
                catch
                {
                    //No Rates Returned
                }
            }

            return hunterExpressServicePrice;
        }

        private async Task<List<HunterExpressServicePriceResultDto>> GetQuote(HunterExpressServicePriceRequestDto priceRequest, int metroZone, string packageType, bool receiverIsResidential)
        {
            try
            {
                HunterExpressAPI hunterExpress = await GetHunterExpressAPIInstance(metroZone, packageType);
                if (hunterExpress != null)
                {
                    priceRequest.customerCode = hunterExpress._hunterExpressAccountNumber;
                    priceRequest.goods = await GetBillableGoodQuantity(priceRequest.goods, Convert.ToDecimal(hunterExpress._hunterExpressMaxArticleWeight));

                    //Price request using the metro zone
                    List<HunterExpressServicePriceResultDto> hunterQuotes = await hunterExpress.GetDomesticQuote(priceRequest);

                    decimal totalSurcharge = 0;
                    Dictionary<string, decimal> hunterSurcharges = new Dictionary<string, decimal>();

                    //Add Business to Customer surcharge if applicable
                    if (receiverIsResidential)
                    {
                        var businessToCustomerSurcharge = await GetBusinessToCustomerSurcharge(priceRequest.goods);
                        if (businessToCustomerSurcharge != null)
                        {
                            hunterSurcharges.Add(businessToCustomerSurcharge.Value.Key, businessToCustomerSurcharge.Value.Value);
                            totalSurcharge += businessToCustomerSurcharge.Value.Value;
                        }
                    }

                    var excessLengthsSurcharge = await GetExcessLengthsSurcharge(priceRequest.goods);
                    if (excessLengthsSurcharge != null)
                    {
                        hunterSurcharges.Add(excessLengthsSurcharge.Value.Key, excessLengthsSurcharge.Value.Value);
                        totalSurcharge += excessLengthsSurcharge.Value.Value;
                    }

                    foreach (var quote in hunterQuotes)
                    {
                        //Add Fuel Cost
                        quote.fee += quote.fee * await GetFuelSurchargeForService(quote.serviceCode);
                        //Add Surcharges
                        //quote.fee += totalSurcharge;
                        quote.Surcharges = hunterSurcharges;
                        //Get total GST charge
                        quote.ReturnedGSTCharge = quote.fee * 0.1m;
                        //Add GST
                        quote.fee = quote.fee * 1.1m;
                        //Add API Id for future use
                        quote.HunterExpressAPIId = hunterExpress._hunterExpressAPIId;
                    }

                    return hunterQuotes;
                }
            }
            catch
            {
                return null;
            }

            return null;
        }

        public async Task<CarrierBookingDto> ValidateConsignment(CarrierBookingDto carrierConsignmentDto)
        {
            try
            {
                string firstPackageType = carrierConsignmentDto.Articles.First().PackageType;
                foreach (var article in carrierConsignmentDto.Articles)
                {
                    if (article.PackageType != firstPackageType)
                    {
                        carrierConsignmentDto.BookingError = 400;
                        carrierConsignmentDto.BookingErrorText = "All articles for Hunter Express consignments must be of the same package type";
                    }
                }
                return carrierConsignmentDto;
            }
            catch
            {
                carrierConsignmentDto.BookingError = 400;
                carrierConsignmentDto.BookingErrorText = "Validation Error";
                return carrierConsignmentDto;
            }
        }

        public async Task<CarrierBookingDto> CreateBooking(CarrierBookingDto bookingInput)
        {
            var hunterExpressAPIAccount = await _hunterExpressAccountsRepository.FirstOrDefaultAsync(a => a.Id == bookingInput.HunterExpressAccountId);
            if (hunterExpressAPIAccount == null)
            {
                Logger.Error("Error booking Customer Order Id: " + bookingInput.OrderId + ". Does not contain a valid HunterExpressAccountId");
                bookingInput.BookingError = 400;
                bookingInput.BookingErrorText = "Booking does not contain a valid Hunter Express Account Id";
                bookingInput.IsBooked = false;

                return bookingInput;
            }

            HunterExpressAPI hunterExpress = await GetHunterExpressAPIInstance(hunterExpressAPIAccount.HunterExpressZoneId, hunterExpressAPIAccount.PackageType);

            if (hunterExpress == null)
            {
                Logger.Error("Error - Could not find Hunter Express API account " + hunterExpressAPIAccount.Description);
                bookingInput.BookingError = 400;
                bookingInput.BookingErrorText = "Carrier API Account Error - Please contact an admin!";
                bookingInput.IsBooked = false;

                return bookingInput;
            }

            //Build Booking DTO
            try
            {
                HunterExpressBookingRequestDto hunterExpressBookingRequest = new HunterExpressBookingRequestDto();

                /* Account Details */
                hunterExpressBookingRequest.customerCode = hunterExpress._hunterExpressAccountNumber;
                hunterExpressBookingRequest.primaryService = bookingInput.ServiceType;
                hunterExpressBookingRequest.reference1 = bookingInput.CustReference;

                /* Sender Details */
                TimeWindow pickUpTime = new TimeWindow();
                pickUpTime.earliest = DateTime.ParseExact(bookingInput.PickupDate, "dd/MM/yyyy", CultureInfo.InvariantCulture).ToString("yyyy-MM-dd") + " " + bookingInput.PickupTime;
                pickUpTime.latest = DateTime.ParseExact(bookingInput.PickupDate, "dd/MM/yyyy", CultureInfo.InvariantCulture).ToString("yyyy-MM-dd") + " " + bookingInput.PickupLatest;

                Contact senderContact = new Contact();
                senderContact.name = bookingInput.SenderName;
                senderContact.phone = bookingInput.SenderPhone;
                senderContact.email = bookingInput.SenderEmailAddress;

                HunterExpressStop senderDetails = new HunterExpressStop();
                senderDetails.name = bookingInput.ContactNameOnLabels ? bookingInput.SenderName : bookingInput.SenderCompanyName;
                senderDetails.suburbName = bookingInput.SenderCity;
                senderDetails.addressLine1 = bookingInput.SenderAddr1;
                senderDetails.addressLine2 = bookingInput.SenderAddr2 + " " + bookingInput.SenderAddr3;
                senderDetails.postCode = bookingInput.SenderPostCode;
                senderDetails.state = bookingInput.SenderState;
                senderDetails.contact = senderContact;
                senderDetails.timeWindow = pickUpTime;

                hunterExpressBookingRequest.stops.Add(senderDetails);

                /*Receiver Details*/
                Contact receiverContact = new Contact();
                receiverContact.name = bookingInput.ReceiverName;
                receiverContact.phone = bookingInput.ReceiverPhone;
                receiverContact.email = bookingInput.ReceiverEmailAddress;

                HunterExpressStop receiverDetails = new HunterExpressStop();

                receiverDetails.name = bookingInput.ReceiverCompanyName.IsNullOrEmpty() ? bookingInput.ReceiverName : bookingInput.ReceiverCompanyName;
                receiverDetails.suburbName = bookingInput.ReceiverCity;
                receiverDetails.addressLine1 = bookingInput.ReceiverAddr1;
                receiverDetails.addressLine2 = bookingInput.ReceiverAddr2 + " " + bookingInput.ReceiverAddr3;
                receiverDetails.postCode = bookingInput.ReceiverPostCode;
                receiverDetails.state = bookingInput.ReceiverState;
                receiverDetails.contact = receiverContact;
                receiverDetails.instructions = bookingInput.Notes;

                hunterExpressBookingRequest.stops.Add(receiverDetails);

                foreach (var article in bookingInput.Articles)
                {
                    HunterExpressGood newGood = new HunterExpressGood();
                    newGood.pieces = 1;
                    newGood.weight = (int)article.Weight;
                    newGood.width = (int)article.Width;
                    newGood.height = (int)article.Height;
                    newGood.depth = (int)article.Length;
                    newGood.typeCode = await GetHunterExpressPackageType(article.PackageType);

                    hunterExpressBookingRequest.goods.Add(newGood);
                }

                var returnedBooking = await hunterExpress.BookDomesticPickup(hunterExpressBookingRequest);

                bookingInput.ConsignmentNumber = returnedBooking.trackingNumber;
                bookingInput.LabelPDF = returnedBooking.shippingLabel;
                bookingInput.CarrierTrackId = returnedBooking.jobNumber;
                bookingInput.IsBooked = true;
            }
            catch (Exception e)
            {
                Logger.Error(e.ToString);
                Logger.Error("DATE " + bookingInput.PickupDate);
                Logger.Error("TIME " + bookingInput.PickupTime);

                bookingInput.IsBooked = false;
                bookingInput.BookingErrorText = e.Message;
                return bookingInput;
            }

            return bookingInput;
        }

        public async Task<HunterExpressStatusResultDto> PollForTrackingUpdates(string trackingNumber, int apiId)
        {
            var apiAccount = await _hunterExpressAccountsRepository.FirstOrDefaultAsync(a => a.Id == apiId);
            if (apiAccount == null)
            {
                return null;
            }

            HunterExpressAPI hunterExpress = await GetHunterExpressAPIInstance(apiAccount.HunterExpressZoneId, apiAccount.PackageType);
            HunterExpressStatusResultDto trackingInfo = new HunterExpressStatusResultDto();
            trackingInfo = await hunterExpress.GetConsignmentTracking(trackingNumber);
            return trackingInfo;
        }

        //Deprecated - Cancellation is now done by sending an email to  support
        public async Task<bool> CancelConsignments(List<HunterExpressRefundDto> orders)
        {
            var contactEmail = await SettingManager.GetSettingValueAsync(AppSettings.General.SupportEmailAddress);
            var contactPhone = await SettingManager.GetSettingValueAsync(AppSettings.General.SupportPhoneNumber);

            foreach (var order in orders)
            {
                try
                {
                    var apiAccount = await _hunterExpressAccountsRepository.FirstOrDefaultAsync(a => a.Id == order.HunterExpressAccountId);
                    if (apiAccount == null)
                    {
                        return false;
                    }

                    HunterExpressAPI hunterExpress = await GetHunterExpressAPIInstance(apiAccount.HunterExpressZoneId, apiAccount.PackageType);
                    hunterExpress._hunterExpressAPIPassword = Encoding.UTF8.GetString(Convert.FromBase64String(apiAccount.CancellationPassword));

                    order.ContactEmail = contactEmail;
                    order.ContactPhone = contactPhone;

                    await hunterExpress.SendConsignmentCancellationRequest(order);
                }
                catch
                {
                    await _emailAppService.EmailAlert("HunterExpress Express Cancellation Error", "Cancellation request for Hunter Express consignment " + order.ConsignmentNumber + " failed to send. Please follow up and manually lodge cancellation.");
                }
            }
            return true;
        }

        /*
         * Helper Methods
         */

        public async Task<string> GetHunterExpressPackageType(string PackageType)
        {
            switch (PackageType.ToUpper())
            {
                case PackageTypeConstants.Satchel:
                    return HunterExpressPackageTypeConstants.Satchel;
                case PackageTypeConstants.Carton:
                    return HunterExpressPackageTypeConstants.Carton;
                case PackageTypeConstants.Pallet:
                    return HunterExpressPackageTypeConstants.Pallet;
                default:
                    return "";
            }
        }

        private async Task<int> GetZoneFromSuburb(string suburb, string postcode)
        {
            try
            {
                var upperCaseSuburb = suburb.ToUpper();
                var record = await _hunterExpressLocationMasterRepository.FirstOrDefaultAsync(r => r.SuburbName == upperCaseSuburb && r.PostCode == postcode);
                return Convert.ToInt32(record.ZoneNumber);
            }
            catch
            {
                return 0;
            }
        }

        private async Task<KeyValuePair<string, decimal>?> GetBusinessToCustomerSurcharge(List<HunterExpressGood> goods)
        {
            decimal weight = 0m;

            foreach (var good in goods)
            {
                weight += await GetTotalConsignmentWeight(new List<HunterExpressGood> { good });
            }

            decimal averageWeightPerItem = weight / goods.Count;

            if (averageWeightPerItem >= 100)
            {
                return new KeyValuePair<string, decimal>(SurchargeConstants.HunterExpress.BusinessToCustomerOver100Kg, 50m);
            }
            else if (averageWeightPerItem >= 75)
            {
                return new KeyValuePair<string, decimal>(SurchargeConstants.HunterExpress.BusinessToCustomer75To99Kg, 30m);
            }
            else if (averageWeightPerItem >= 50)
            {
                return new KeyValuePair<string, decimal>(SurchargeConstants.HunterExpress.BusinessToCustomer50To74Kg, 20m);
            }
            else
            {
                return null;
            }

        }

        private async Task<KeyValuePair<string, decimal>?> GetExcessLengthsSurcharge(List<HunterExpressGood> goods)
        {
            decimal totalSurcharge = 0m;

            foreach (var good in goods)
            {
                decimal goodWidth = Convert.ToDecimal(good.width);
                decimal goodLength = Convert.ToDecimal(good.depth);
                decimal goodHeight = Convert.ToDecimal(good.height);

                if (goodWidth >= 600 || goodLength >= 600 || goodHeight >= 600)
                {
                    throw new Exception("Excessive dimensions over 6m are not allowed.");
                }
                else if (goodWidth >= 400 || goodLength >= 400 || goodHeight >= 400)
                {
                    return new KeyValuePair<string, decimal>(SurchargeConstants.HunterExpress.ExcessLengthsOver400Cm, 100m);
                }
                else if (goodWidth >= 250 || goodLength >= 250 || goodHeight >= 250)
                {
                    return new KeyValuePair<string, decimal>(SurchargeConstants.HunterExpress.ExcessLengthsOver250Cm, 50m);
                }
            }
            return null;
        }

        private async Task<decimal> GetTotalConsignmentWeight(List<HunterExpressGood> goods)
        {
            decimal deadWeight = 0;
            decimal cubicWeight = 0;

            decimal totalLength = 0;
            decimal totalWidth = 0;
            decimal totalHeight = 0;

            foreach (var good in goods)
            {
                deadWeight += good.weight;
                totalHeight += good.height;
                totalLength += good.depth;
                totalWidth += good.width;
            }

            cubicWeight = (totalHeight * totalLength * totalWidth) / 4000;

            if (deadWeight > cubicWeight)
            {
                return deadWeight;
            }
            else
            {
                return cubicWeight;
            }
        }

        private async Task<List<HunterExpressGood>> GetBillableGoodQuantity(List<HunterExpressGood> goods, decimal maxGoodWeight)
        {
            foreach (var good in goods)
            {
                if (good.typeCode == HunterExpressPackageTypeConstants.Carton)
                {
                    int goodQuantity = Convert.ToInt32(good.pieces);
                    decimal goodWeight = await GetTotalConsignmentWeight(new List<HunterExpressGood> { good });
                    int calculatedQuantity = Convert.ToInt32(Math.Ceiling(goodWeight / maxGoodWeight));
                    if (goodQuantity < calculatedQuantity)
                    {
                        good.pieces = calculatedQuantity;
                    }
                }
                else if (good.typeCode == HunterExpressPackageTypeConstants.Pallet)
                {
                    var pallet = await _packageTypeRepository.FirstOrDefaultAsync(p => p.Name.ToUpper() == PackageTypeConstants.Pallet);

                    decimal numPalletsWide = Math.Ceiling(Convert.ToDecimal(good.width) / pallet.DefaultWidth);
                    decimal numPalletsLong = Math.Ceiling(Convert.ToDecimal(good.depth) / pallet.DefaultLength);
                    decimal numPalletsHigh = Math.Ceiling(Convert.ToDecimal(good.height) / pallet.DefaultHeight);

                    int totalChargeablePalletsByDims = (int)new decimal[] { numPalletsWide, numPalletsLong, numPalletsHigh }.Max();
                    int totalChargeablePalletsByWeight = (int)Math.Ceiling(Convert.ToDecimal(good.weight) / pallet.MaxWeight);

                    good.pieces = totalChargeablePalletsByWeight > totalChargeablePalletsByDims ? totalChargeablePalletsByWeight : totalChargeablePalletsByDims;
                }
            }

            return goods;
        }

        private async Task<decimal> GetFuelSurchargeForService(string serviceName)
        {
            //Only same day courier services incur the courier surcharge. All other services incur the standard surcharge.
            switch (serviceName.ToUpper())
            {
                case HunterExpressServiceTypeConstants.RoadFreight:
                    return Convert.ToDecimal(await SettingManager.GetSettingValueAsync(HunterExpressSettingsNames.HunterExpressStandardFuelSurcharge));
                case HunterExpressServiceTypeConstants.AirFreight:
                    return Convert.ToDecimal(await SettingManager.GetSettingValueAsync(HunterExpressSettingsNames.HunterExpressStandardFuelSurcharge));
                case HunterExpressServiceTypeConstants.HomeDeliveryPlus:
                    return Convert.ToDecimal(await SettingManager.GetSettingValueAsync(HunterExpressSettingsNames.HunterExpressStandardFuelSurcharge));
                case HunterExpressServiceTypeConstants.SameDayAirFreight:
                    return Convert.ToDecimal(await SettingManager.GetSettingValueAsync(HunterExpressSettingsNames.HunterExpressStandardFuelSurcharge));
                default:
                    return Convert.ToDecimal(await SettingManager.GetSettingValueAsync(HunterExpressSettingsNames.HunterExpressStandardFuelSurcharge));

            }
        }
    }
}